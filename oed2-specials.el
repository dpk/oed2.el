;; -*- lexical-binding: t; -*-
;;;
;; This file is derived from
;; /usr/web/sources/plan9/sys/src/cmd/dict/oed.c
;; in the Plan 9 from Bell Labs source tree.
;;
;; Licence information for that original file:
;; <https://9p.io/plan9/license.html>

(defvar oed2-specials
  '(("3on4" . "¾")
    ("Aacu" . "Á")
    ("Aang" . "Å")
    ("Abarab" . "Ā")
    ("Acirc" . "Â")
    ("Ae" . "Æ")
    ("Agrave" . "À")
    ("Alpha" . "Α")
    ("Amac" . "Ā")
    ("Asg" . "Ᵹ")
    ("Auml" . "Ä")
    ("Beta" . "Β")
    ("Cced" . "Ç")
    ("Chacek" . "Č")
    ("Chi" . "Χ")
    ("Chirho" . "☧")
    ("Csigma" . "Ϛ")
    ("Delta" . "Δ")
    ("Eacu" . "É")
    ("Ecirc" . "Ê")
    ("Edh" . "Ð")
    ("Epsilon" . "Ε")
    ("Eta" . "Η")
    ("Gamma" . "Γ")
    ("Iacu" . "Í")
    ("Icirc" . "Î")
    ("Imac" . "Ī")
    ("Integ" . "∫")
    ("Iota" . "Ι")
    ("Kappa" . "Κ")
    ("Koppa" . "Ϟ")
    ("Lambda" . "Λ")
    ("Lbar" . "Ł")
    ("Mu" . "Μ")
    ("Naira" . "N") ;; should have bar through
    ("Nplus" . "N") ;; should have plus above
    ("Ntilde" . "Ñ")
    ("Nu" . "Ν")
    ("Oacu" . "Ó")
    ("Obar" . "Ø")
    ("Ocirc" . "Ô")
    ("Oe" . "Œ")
    ("Omega" . "Ω")
    ("Omicron" . "Ο")
    ("Ouml" . "Ö")
    ("Phi" . "Φ")
    ("Pi" . "Π")
    ("Psi" . "Ψ")
    ("Rho" . "Ρ")
    ("Sacu" . "Ś")
    ("Sigma" . "Σ")
    ("Summ" . "∑")
    ("Tau" . "Τ")
    ("Th" . "Þ")
    ("Theta" . "Θ")
    ("Tse" . "Ц")
    ("Uacu" . "Ú")
    ("Ucirc" . "Û")
    ("Upsilon" . "Υ")
    ("Uuml" . "Ü")
    ("Wyn" . "Ƿ")
    ("Xi" . "Ξ")
    ("Ygh" . "Ȝ")
    ("Zeta" . "Ζ")
    ("Zh" . "Ʒ")
    ("a" . "a") ;; ante; should be italic
    ("aacu" . "á")
    ("aang" . "å")
    ("aasper" . "ἁ")
    ("abreve" . "ă")
    ("acirc" . "â")
    ("acu" . "\u0301")
    ("ae" . "æ")
    ("agrave" . "à")
    ("ahook" . "ą")
    ("alenis" . "ἀ")
    ("alpha" . "α")
    ("amac" . "ā")
    ("amp" . "&")
    ("and" . "and") ;; should be roman type
    ("ang" . "\u030A")
    ("angle" . "∠")
    ("ankh" . "☥")
    ("appreq" . "≃")
    ("aquar" . "♒")
    ("arDadfull" . "ض") ;; Dad U+0636 
    ("arHa" . "ح") ;; haa U+062D 
    ("arTa" . "ت") ;; taa U+062A 
    ("arain" . "ع") ;; ain U+0639 
    ("arainfull" . "ع") ;; ain U+0639 
    ("aralif" . "ا") ;; alef U+0627 
    ("arba" . "ب") ;; baa U+0628 
    ("arha" . "ه") ;; ha U+0647 
    ("aries" . "♈")
    ("arnun" . "ن") ;; noon U+0646 
    ("arnunfull" . "ن") ;; noon U+0646 
    ("arpa" . "ه") ;; ha U+0647 
    ("arqoph" . "ق") ;; qaf U+0642 
    ("arshinfull" . "ش") ;; sheen U+0634 
    ("arta" . "ت") ;; taa U+062A 
    ("artafull" . "ت") ;; taa U+062A 
    ("artha" . "ث") ;; thaa U+062B 
    ("arwaw" . "و") ;; waw U+0648 
    ("arya" . "ي") ;; ya U+064A 
    ("aryafull" . "ي") ;; ya U+064A 
    ("arzero" . "٠") ;; indic zero U+0660 
    ("asg" . "ᵹ")
    ("asper" . "\u0314")
    ("assert" . "⊢")
    ("astm" . "⁂") ;; 'asterism: should be upside down' (?)
    ("at" . "@")
    ("atilde" . "ã")
    ("auml" . "ä")
    ("ayin" . "ع") ;; arabic ain U+0639 
    ("b1" . "-")
    ("b2" . "=")
    ("b3" . "≡")
    ("bbar" . "ƀ")
    ("beta" . "β")
    ("bigobl" . "/")
    ("blC" . "C") ;; should be black letter
    ("blJ" . "J") ;; should be black letter
    ("blU" . "U") ;; should be black letter
    ("blb" . "b") ;; should be black letter
    ("blozenge" . "⬧")
    ("bly" . "y") ;; should be black letter
    ("brbl" . "\u032E")
    ("breve" . "\u0306")
    ("bslash" . "\\")
    ("bsquare" . "■")
    ("btril" . "◀")
    ("btrir" . "▶")
    ("c" . "c") ;; circa; should be italic
    ("cab" . "〉")
    ("cacu" . "ć")
    ("canc" . "♋")
    ("capr" . "♑")
    ("caret" . "^")
    ("cb" . "}")
    ("cbigb" . "}")
    ("cbigpren" . ")")
    ("cbigsb" . "]")
    ("cced" . "ç")
    ("cdil" . "\u0327")
    ("cdsb" . "〛")
    ("cent" . "¢")
    ("chacek" . "č")
    ("chi" . "χ")
    ("circ" . "\u0302")
    ("circbl" . "\u0325")
    ("circle" . "○")
    ("circledot" . "⊙")
    ("click" . "ʖ")
    ("club" . "♣")
    ("comtime" . "C")
    ("conj" . "☌")
    ("cprt" . "©")
    ("cq" . "’")
    ("cqq" . "”")
    ("cross" . "✠")
    ("crotchet" . "♩")
    ("csb" . "]")
    ("ctilde" . "c̃")
    ("ctlig" . "ct") ;; should be ligature
    ("cyra" . "а")
    ("cyre" . "е")
    ("cyrhard" . "ъ")
    ("cyrjat" . "ѣ")
    ("cyrm" . "м")
    ("cyrn" . "н")
    ("cyrr" . "р")
    ("cyrsoft" . "ь")
    ("cyrt" . "т")
    ("cyry" . "ы")
    ("dag" . "†")
    ("dbar" . "đ")
    ("dblar" . "⇋")
    ("dblgt" . "≫")
    ("dbllt" . "≪")
    ("dced" . "ḑ")
    ("dd" . "..")
    ("ddag" . "‡")
    ("ddd" . "...")
    ("decr" . "↓")
    ("deg" . "°")
    ("delta" . "δ")
    ("descnode" . "☋") ;; descending node U+260B 
    ("diamond" . "♢")
    ("digamma" . "ϝ")
    ("div" . "÷")
    ("dlessi" . "ı")
    ("dlessj1" . "j") ;; should be dotless 
    ("dlessj2" . "j") ;; should be dotless 
    ("dlessj3" . "j") ;; should be dotless 
    ("dollar" . "$")
    ("dotab" . "\u0307")
    ("dotbl" . "\u0323")
    ("drachm" . "ʒ")
    ("dubh" . "-")
    ("eacu" . "é")
    ("earth" . "♁")
    ("easper" . "ἑ")
    ("ebreve" . "ĕ")
    ("ecirc" . "ê")
    ("edh" . "ð")
    ("egrave" . "è")
    ("ehacek" . "ě")
    ("ehook" . "ę")
    ("elem" . "∊")
    ("elenis" . "ἐ")
    ("em" . "—")
    ("emac" . "ē")
    ("emem" . "——")
    ("en" . "–")
    ("epsilon" . "ε")
    ("equil" . "⇋")
    ("ergo" . "∴")
    ("eszett" . "ß")
    ("eta" . "η")
    ("eth" . "ð")
    ("euml" . "ë")
    ("expon" . "↑")
    ("fact" . "!")
    ("fata" . "ɑ")
    ("fatpara" . "❡")
    ("female" . "♀")
    ("ffilig" . "ﬃ")
    ("fflig" . "ﬀ")
    ("ffllig" . "ﬄ")
    ("filig" . "ﬁ")
    ("flat" . "♭")
    ("fllig" . "ﬂ")
    ("frE" . "E") ;; "should be curly"
    ("frL" . "L") ;; "should be curly"
    ("frR" . "R") ;; "should be curly"
    ("frakB" . "𝔅")
    ("frakG" . "𝔊")
    ("frakH" . "𝕳")
    ("frakI" . "𝕴")
    ("frakM" . "𝔐")
    ("frakU" . "𝔘")
    ("frakX" . "𝔛")
    ("frakY" . "𝔜")
    ("frakh" . "𝔥")
    ("frbl" . "\u032F")
    ("frown" . "\u0311")
    ("fs" . " ")
    ("fsigma" . "ς")
    ("gAacu" . "Ά")		
    ("gaacu" . "ά")       
    ("gabreve" . "ᾰ")
    ("gafrown" . "ᾶ")
    ("gagrave" . "ὰ")
    ("gamac" . "ᾱ")
    ("gamma" . "γ")
    ("gauml" . "α̈")
    ("ge" . "≧")
    ("geacu" . "έ")
    ("gegrave" . "ὲ")
    ("ghacu" . "ή")
    ("ghfrown" . "ῆ")
    ("ghgrave" . "ὴ")
    ("ghmac" . "η̄")	
    ("giacu" . "ί")	
    ("gibreve" . "ῐ")	
    ("gifrown" . "ῖ")	
    ("gigrave" . "ὶ")	
    ("gimac" . "ῑ")	
    ("giuml" . "ϊ")	
    ("glagjat" . "ѧ")
    ("glots" . "ˀ")
    ("goacu" . "ό")
    ("gobreve" . "ŏ")
    ("grave" . "\u0300")
    ("gt" . ">")
    ("guacu" . "ύ")
    ("gufrown" . "ῦ")
    ("gugrave" . "ὺ")
    ("gumac" . "ῡ")
    ("guuml" . "ϋ")
    ("gwacu" . "ώ")
    ("gwfrown" . "ῶ")
    ("gwgrave" . "ὼ")
    ("hacek" . "\u030C")
    ("halft" . "⌈")
    ("hash" . "#")
    ("hasper" . "ἡ")
    ("hatpath" . "ֲ") ;; hataf patah U+05B2 
    ("hatqam" . "ֳ") ;; hataf qamats U+05B3 
    ("hatseg" . "ֱ") ;; hataf segol U+05B1 
    ("hbar" . "ħ")
    ("heart" . "♡")
    ("hebaleph" . "א") ;; aleph U+05D0 
    ("hebayin" . "ע") ;; ayin U+05E2 
    ("hebbet" . "ב") ;; bet U+05D1 
    ("hebbeth" . "ב") ;; bet U+05D1 
    ("hebcheth" . "ח") ;; bet U+05D7 
    ("hebdaleth" . "ד") ;; dalet U+05D3 
    ("hebgimel" . "ג") ;; gimel U+05D2 
    ("hebhe" . "ה") ;; he U+05D4 
    ("hebkaph" . "כ") ;; kaf U+05DB 
    ("heblamed" . "ל") ;; lamed U+05DC 
    ("hebmem" . "מ") ;; mem U+05DE 
    ("hebnun" . "נ") ;; nun U+05E0 
    ("hebnunfin" . "ן") ;; final nun U+05DF 
    ("hebpe" . "פ") ;; pe U+05E4 
    ("hebpedag" . "ף") ;; final pe? U+05E3 
    ("hebqoph" . "ק") ;; qof U+05E7 
    ("hebresh" . "ר") ;; resh U+05E8 
    ("hebshin" . "ש") ;; shin U+05E9 
    ("hebtav" . "ת") ;; tav U+05EA 
    ("hebtsade" . "צ") ;; tsadi U+05E6 
    ("hebwaw" . "ו") ;; vav? U+05D5 
    ("hebyod" . "י") ;; yod U+05D9 
    ("hebzayin" . "ז") ;; zayin U+05D6 
    ("hgz" . "ʒ") ;; ??? Cf "alet" 
    ("hireq" . "ִ") ;; U+05B4 
    ("hlenis" . "ἠ")
    ("hook" . "\u0328")
    ("horizE" . "E") ;; should be on side
    ("horizP" . "P") ;; should be on side
    ("horizS" . "∽")
    ("horizT" . "⊣")
    ("horizb" . "⏟") ;; should be underbrace
    ("ia" . "α")
    ("iacu" . "í")
    ("iasper" . "ἱ")
    ("ib" . "β")
    ("ibar" . "ɨ")
    ("ibreve" . "ĭ")
    ("icirc" . "î")
    ("id" . "δ")
    ("ident" . "≡")
    ("ie" . "ε")
    ("ifilig" . "ﬁ") ;; should be italic
    ("ifflig" . "ﬀ") ;; should be italic
    ("ig" . "γ")
    ("igrave" . "ì")
    ("ih" . "η")
    ("ii" . "ι")
    ("ik" . "κ")
    ("ilenis" . "ἰ")
    ("imac" . "ī")
    ("implies" . "⇒")
    ("index" . "☞")
    ("infin" . "∞")
    ("integ" . "∫")
    ("intsec" . "∩")
    ("invpri" . "ˏ")
    ("iota" . "ι")
    ("iq" . "ψ")
    ("istlig" . "ﬆ") ;; should be italic
    ("isub" . "\u0345")
    ("iuml" . "ï")
    ("iz" . "ζ")
    ("jup" . "♃")
    ("kappa" . "κ")
    ("koppa" . "ϟ")
    ("lambda" . "λ")
    ("lar" . "←")
    ("lbar" . "ł")
    ("le" . "≦")
    ("lenis" . "\u0313")
    ("leo" . "♌")
    ("lhalfbr" . "⌈")
    ("lhshoe" . "⊃")
    ("libra" . "♎")
    ("lm" . "ː")
    ("logicand" . "∧")
    ("logicor" . "∨")
    ("longs" . "ʃ")
    ("lrar" . "↔")
    ("lt" . "<")
    ("ltappr" . "≾")
    ("ltflat" . "∠")
    ("lumlbl" . "l̤")
    ("mac" . "\u0304")
    ("male" . "♂")
    ("mc" . "c") ;; should be raised 
    ("merc" . "☿") ;; mercury U+263F 
    ("min" . "−")
    ("moonfq" . "☽") ;; first quarter moon U+263D 
    ("moonlq" . "☾") ;; last quarter moon U+263E 
    ("msylab" . "m̩")
    ("mu" . "μ")
    ("nacu" . "ń")
    ("natural" . "♮")
    ("neq" . "≠")
    ("nfacu" . "′")
    ("nfasper" . "ʽ")
    ("nfbreve" . "˘")
    ("nfced" . "¸")
    ("nfcirc" . "ˆ")
    ("nffrown" . "⌢")
    ("nfgra" . "ˋ")
    ("nfhacek" . "ˇ")
    ("nfmac" . "¯")
    ("nftilde" . "˜")
    ("nfuml" . "¨")
    ("ng" . "ŋ")
    ("not" . "¬")
    ("notelem" . "∉")
    ("ntilde" . "ñ")
    ("nu" . "ν")
    ("oab" . "〈")
    ("oacu" . "ó")
    ("oasper" . "ὁ")
    ("ob" . "{")
    ("obar" . "ø")
    ("obigb" . "{") ;; should be big
    ("obigpren" . "(")
    ("obigsb" . "[") ;; should be big
    ("obreve" . "ŏ")
    ("ocirc" . "ô")
    ("odsb" . "〚")
    ("oe" . "œ")
    ("oeamp" . "&")
    ("ograve" . "ò")
    ("ohook" . "ǫ")
    ("olenis" . "ὀ")
    ("omac" . "ō")
    ("omega" . "ω")
    ("omicron" . "ο")
    ("ope" . "ɛ")
    ("opp" . "☍")
    ("oq" . "‘")
    ("oqq" . "“")
    ("or" . "or") ;; should be roman type
    ("osb" . "[")
    ("otilde" . "õ")
    ("ouml" . "ö")
    ("ounce" . "℥")
    ("ovparen" . "⌢") ;; should be sideways
    ("p" . "′")
    ("pa" . "∂")
    ("page" . "P")
    ("pall" . "ʎ")
    ("paln" . "ɲ")
    ("para" . "¶")
    ("pbar" . "ᵽ")
    ("per" . "℘")
    ("phi" . "φ")
    ("phi2" . "ϕ")
    ("pi" . "π")
    ("pisces" . "♓")
    ("planck" . "ħ")
    ("plantinJ" . "𝐽") ;; should be script?
    ("pm" . "±")
    ("pmil" . "‰")
    ("pp" . "″")
    ("ppp" . "‴")
    ("prop" . "∝")
    ("psi" . "ψ")
    ("pstlg" . "£")
    ("q" . "?") ;; 'should be raised'
    ("qamets" . "ֳ")
    ("quaver" . "♪")
    ("rar" . "→")
    ("rasper" . "ῥ")
    ("rdot" . "·")
    ("recipe" . "℞")
    ("reg" . "®")
    ("revC" . "Ɔ")
    ("reva" . "ɒ")
    ("revc" . "ɔ")
    ("revope" . "ɜ")
    ("revr" . "ɹ")
    ("revsc" . "˒")
    ("revv" . "ʌ")
    ("rfa" . "ǫ")
    ("rhacek" . "ř")
    ("rhalfbr" . "⌉")
    ("rho" . "ρ")
    ("rhshoe" . "⊂")
    ("rlenis" . "ῤ")
    ("rsylab" . "r̩")
    ("runash" . "ᚫ")
    ("rvow" . "˔")
    ("sacu" . "ś")
    ("sagit" . "♐")
    ("sampi" . "ϡ")
    ("saturn" . "♄")
    ("sced" . "ş")
    ("schwa" . "ə")
    ("scorpio" . "♏")
    ("scrA" . "𝒜")
    ("scrC" . "𝒞")
    ("scrE" . "ℰ")
    ("scrF" . "ℱ")
    ("scrI" . "ℐ")
    ("scrJ" . "𝒥")
    ("scrL" . "ℒ")
    ("scrO" . "𝒪")
    ("scrP" . "𝒫")
    ("scrQ" . "𝒬")
    ("scrS" . "𝒮")
    ("scrT" . "𝒯")
    ("scrb" . "𝒷")
    ("scrd" . "𝒹")
    ("scrh" . "𝒽")
    ("scrl" . "𝓁")
    ("scruple" . "℈")
    ("sdd" . "ː")
    ("sect" . "§")
    ("semE" . "∃")
    ("sh" . "ʃ")
    ("shacek" . "š")
    ("sharp" . "♯")
    ("sheva" . "ְ")
    ("shti" . "ɪ")
    ("shtsyll" . "∪")
    ("shtu" . "ʊ")
    ("sidetri" . "⊲")
    ("sigma" . "σ")
    ("since" . "∵")
    ("slge" . "⩾")
    ("slle" . "⩽")
    ("sm" . "ˈ")
    ("smm" . "ˌ")
    ("spade" . "♠")
    ("sqrt" . "√")
    ("square" . "□")
    ("ssChi" . "𝝬")
    ("ssIota" . "𝝞")
    ("ssOmicron" . "𝝤")
    ("ssPi" . "𝝥")
    ("ssRho" . "𝝦")
    ("ssSigma" . "𝝨")
    ("ssTau" . "𝝩")
    ("star" . "*")
    ("stlig" . "ﬆ")
    ("supgt" . "˃")
    ("suplt" . "˂")
    ("sur" . "ʳ")
    ("swing" . "∼")
    ("tau" . "τ")
    ("taur" . "♉")
    ("th" . "þ")
    ("thbar" . "ꝥ")
    ("theta" . "θ")
    ("thinqm" . "?") ;; should be thinner
    ("tilde" . "\u0303")
    ("times" . "×")
    ("tri" . "∆")
    ("trli" . "‖")
    ("ts" . " ")
    ("uacu" . "ú")
    ("uasper" . "ὑ")
    ("ubar" . "ʉ")
    ("ubreve" . "ŭ")
    ("ucirc" . "û")
    ("udA" . "∀")
    ("udT" . "⊥")
    ("uda" . "ɐ")
    ("udh" . "ɥ")
    ("udqm" . "¿")
    ("udpsi" . "⋔")
    ("udtr" . "∇")
    ("ugrave" . "ù")
    ("ulenis" . "ὐ")
    ("umac" . "ū")
    ("uml" . "\u0308")
    ("undl" . "\u0332")
    ("union" . "∪")
    ("upsilon" . "υ")
    ("uuml" . "ü")
    ("vavpath" . "ו") ;; vav U+05D5 (+patah) 
    ("vavsheva" . "ו") ;; vav U+05D5 (+sheva) 
    ("vb" . "|")
    ("vddd" . "⋮")
    ("versicle2" . "℣")
    ("vinc" . "¯")
    ("virgo" . "♍")
    ("vpal" . "ɟ")
    ("vvf" . "ɣ")
    ("wasper" . "ὡ")
    ("wavyeq" . "≈")
    ("wlenis" . "ὠ")
    ("wyn" . "ƿ")
    ("xi" . "ξ")
    ("yacu" . "ý")
    ("ycirc" . "ŷ")
    ("ygh" . "ȝ")
    ("ymac" . "ȳ")
    ("yuml" . "ÿ")
    ("zced" . "z̧")
    ("zeta" . "ζ")
    ("zh" . "ʒ")
    ("zhacek" . "ž"))
  "Alist mapping the names of special characters in the OED2 to the
Unicode characters (or strings) they should display as in OED2
mode.")

(defvar oed2-ipa-transcription
  '(("\"" . "ˈ")
    ("%" . "ˌ")
    ("&" . "æ")
    ("3" . "ɜ")
    ("9" . "ø")
    (":" . "ː")
    ("@" . "ə")
    ("A" . "ɑ")
    ("D" . "ð")
    ("E" . "ɛ")
    ("I" . "ɪ")
    ("N" . "ŋ")
    ("O" . "ɔ")
    ("Q" . "ɒ")
    ("R" . "")
    ("S" . "ʃ")
    ("T" . "θ")
    ("U" . "ʊ")
    ("V" . "ʌ")
    ("Z" . "ʒ"))
  "Maps ASCII characters used in the OED2 <ph> tags to their
corresponding Unicode IPA ones.")

(defvar oed2-greek-transcription
  '(("A" . "Α")
    ("B" . "Β")
    ("C" . "Ξ")
    ("D" . "Δ")
    ("E" . "Ε")
    ("F" . "Φ")
    ("G" . "Γ")
    ("H" . "Η")
    ("I" . "Ι")
    ("J" . "Ϛ")
    ("K" . "Κ")
    ("L" . "Λ")
    ("M" . "Μ")
    ("N" . "Ν")
    ("O" . "Ο")
    ("P" . "Π")
    ("Q" . "Θ")
    ("R" . "Ρ")
    ("S" . "Σ")
    ("T" . "Τ")
    ("U" . "Υ")
    ("W" . "Ω")
    ("X" . "Χ")
    ("Y" . "Ψ")
    ("Z" . "Ζ")
    ("a" . "α")
    ("b" . "β")
    ("c" . "ξ")
    ("d" . "δ")
    ("e" . "ε")
    ("f" . "φ")
    ("g" . "γ")
    ("h" . "η")
    ("i" . "ι")
    ("j" . "ς")
    ("k" . "κ")
    ("l" . "λ")
    ("m" . "μ")
    ("n" . "ν")
    ("o" . "ο")
    ("p" . "π")
    ("q" . "θ")
    ("r" . "ρ")
    ("s" . "σ")
    ("t" . "τ")
    ("u" . "υ")
    ("w" . "ω")
    ("x" . "χ")
    ("y" . "ψ")
    ("z" . "ζ"))
  "Maps ASCII characters used in the OED2 <gk> tags to their
corresponding Unicode Greek ones.")

(provide 'oed2-specials)

;; in addition to those listed in oed.c (some of which may now be in
;; Unicode, which needs to be checked), the following have no Unicode
;; equivalent here:
;;
;; aonq
;; bra
;; dele
;; llswing
